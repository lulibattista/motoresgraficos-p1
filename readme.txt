Classless.Hasher
.NET Hash and Checksum Algorithm Library
(c)2004-2009 Classless.net
http://code.google.com/p/classless-hasher/

The source code of this library can be obtained from:
http://code.google.com/p/classless-hasher/