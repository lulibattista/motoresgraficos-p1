using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public GameObject Jugador;
    public Vector3 offset;
    public Vector2 mouseMirar;
    public Vector2 suavidadV;
    public float movMouseX;
    public float sensisuav;

    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;

    public float speedH;
    public float speedV;

    float yaw;
    float pitch;






    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - Jugador.transform.position;
        Jugador = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {

        //var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        movMouseX = Input.GetAxisRaw("Mouse X");
        var md = new Vector2(movMouseX, 0);
        //var md = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        sensisuav = sensibilidad * suavizado;
        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90, 90f);
        
        transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, Vector3.up);
        Jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, Jugador.transform.up);

        /*yaw += speedH * Input.GetAxis("Mouse X");
        pitch += speedV * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);*/





    }




    void LateUpdate()
    {
        transform.position = Jugador.transform.position + offset;
    }
}
