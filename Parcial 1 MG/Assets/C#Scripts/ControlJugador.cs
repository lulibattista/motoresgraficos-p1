using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Mime;
//using System.Diagnostics;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using TMPro;

//using System.Diagnostics;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
    public float movimientoAdelanteAtras;
    public float movimientoCostados;
    public int rapidez = 10;
    public float X_inicial;
    public float Z_inicial;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public SphereCollider col;
    public float sphereRadious;
    public Camera camaraProyectil;
    public GameObject bala;
    public int cantidadEnemigos = 5;
    public int cantidadPastilla = 1;
    public TMP_Text ElTexto;
    public TMP_Text TextoInfo;
    public TMP_Text Pildora;
    


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        magnitudSalto = 7;
        col = GetComponent<SphereCollider>();
        X_inicial = transform.position.x;
        Z_inicial = transform.position.z;
        Cursor.lockState = CursorLockMode.Locked;
        transform.Translate(0, 0, 0);
        rapidez = 20;
        cont = 0;
        //textoGanaste.text = "";
        setearTextos();
        
    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = " Cantidad recolectados: " + cont.ToString();
        if (cont >= 5)
        {
            textoGanaste.text = "Ganaste!";

        }
    }

    

    
    // Update is called once per frame
    void Update()
    {
        movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidez;
        movimientoCostados = Input.GetAxis("Horizontal") * rapidez;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up*magnitudSalto, ForceMode.Impulse);
        }

        else if((Input.GetKeyDown(KeyCode.R))||( transform.position.y < 0))
        {
            transform.position = new Vector3(X_inicial, 0f, Z_inicial);
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraProyectil.ViewportPointToRay(new Vector3(0.5f, 05f, 0));
            RaycastHit hit;
            //Debug.Log("Toca el botton izquierdo " );
            
            if ((Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward),out hit) ==true))
            {
                //Debug.Log("El rayo toco al objeto " + hit.collider.name);
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                ControlEnemigo1 scriptobjetoTocado = (ControlEnemigo1)objetoTocado.GetComponent(typeof(ControlEnemigo1));
                if(scriptobjetoTocado!= null)
                {
                    scriptobjetoTocado.Liquidar();
                    cantidadEnemigos -= 1;
                    if (cantidadEnemigos <= 0)
                    {
                        ElTexto.fontSize = 30;
                        ElTexto.text = " Ganaste! ";
                        
                    }

                }
            }
            GameObject balita;
            balita = Instantiate(bala, ray.origin , transform.rotation);
            Rigidbody ae = balita.GetComponent<Rigidbody>();
            ae.AddForce(camaraProyectil.transform.forward * 20, ForceMode.Impulse);
            
            Destroy(balita, 3);


        }
        TextoInfo.text = " Enemigos: " + cantidadEnemigos;
        Pildora.text = " Pastilla: " + cantidadPastilla;
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckSphere(transform.position, col.radius *.9f,capaPiso);
        //return false;
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.name == "item")
        {
            cont = cont + 1;
            Debug.Log(" Se drogo ");
            other.gameObject.SetActive(false);
            rapidez += 5;
            transform.localScale += new Vector3(1f, 1f, 1f);
            cantidadPastilla -= 1;
        }

    }
    
}
