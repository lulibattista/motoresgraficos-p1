using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
//using System.Diagnostics;
using UnityEngine;


public class ControlEnemigo1 : MonoBehaviour
{

    bool tengoQueMoverDer= true;
    bool tengoQueSubir = true;
    bool tengoqueMoverAde=true;
    int rapidez = 10;
    int rapidez2 = 2;
    int rapidez3 = 3;
    public GameObject jugadorrr;
    

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (this.name == "Enemigo 2")
        {
            if (transform.position.x <= 30 && tengoQueMoverDer == true)
            {
                tengoQueMoverDer = true;
            }

            if (transform.position.x > 30 && tengoQueMoverDer == true)
            {
                tengoQueMoverDer = false;
            }
            if (transform.position.x <= -30 && tengoQueMoverDer == false)
            {
                tengoQueMoverDer = true;
            }

            if (transform.position.x > -30 && tengoQueMoverDer == false)
            {
                tengoQueMoverDer = false;
            }


            if (tengoQueMoverDer)
            {
                Derecha();

            }
            else
            {
                Izquierda();
            }
        }

        if(this.name == "Enemigo 1")
        {
            if (transform.position.y <= 5 && tengoQueSubir == true)
            {
                tengoQueSubir = true;
            }

            if (transform.position.y > 5 && tengoQueSubir == true)
            {
                tengoQueSubir = false;
            }

            if (transform.position.y <= 1.5 && tengoQueSubir == false)
            {
                tengoQueSubir = true;
            }

            if (transform.position.y >= 1.5 && tengoQueSubir == false)
            {
                tengoQueSubir = false;
            }


            if (tengoQueSubir)
            {
                Subir();
            }

            else
            {
                Bajar();
            }
        }
        if (this.name == "Enemigo 3")
        {
            //transform.position = new Vector3(jugadorrr.transform.position.x + 10, transform.position.y, jugadorrr.transform.position.z + 10);
            transform.LookAt(jugadorrr.transform);
            transform.Translate(rapidez3 * Vector3.forward * Time.deltaTime);
        }
        if (this.name == "Enemigo 4")
        {
            if (transform.position.z <= 50 && tengoqueMoverAde == true)
            {
                tengoqueMoverAde = true;
            }

            if (transform.position.z > 50 && tengoqueMoverAde == true)
            {
                tengoqueMoverAde = false;
            }
            if (transform.position.z <= -50 && tengoqueMoverAde == false)
            {
                tengoqueMoverAde = true;
            }

            if (transform.position.z > -50 && tengoqueMoverAde == false)
            {
                tengoqueMoverAde = false;
            }


            if (tengoqueMoverAde)
            {
                Adelante();

            }
            else
            {
                Atras();
            }
        }

    }

    void Izquierda()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

    void Derecha()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez2 * Time.deltaTime;
    }

    void Subir()
    {
        transform.position += transform.up * rapidez2 * Time.deltaTime;
    }
    void Atras()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }

    void Adelante()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }



    public void Liquidar()
    {
        Destroy(gameObject, 2);
        
        
    }
}

