using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
//using System.Runtime.Hosting;
//using System.Diagnostics;
using UnityEngine;
using TMPro;


public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public float temporizador = 0;
    public TMP_Text ElTexto;
    

    // Start is called before the first frame update
    void Start()
    {

        ComenzarJuego();

        //Debug.Log("Hace Algo ");
        StartCoroutine("ComenzarCronometro");
        
    }
    void ComenzarJuego()
    {
        

    }
    // Update is called once per frame
    void Update()
    {
       
    }
    public IEnumerator ComenzarCronometro()
    {
        temporizador=60;
        //Debug.Log(" Restan " + temporizador + " segundos");
        
        while (temporizador > 0)
        {
            if (ElTexto.text != " Ganaste! ")
            {

                ElTexto.text = " Restan " + temporizador + " segundos";

            }
            yield return new WaitForSeconds(1.0f);
            temporizador--;
        }
        ElTexto.fontSize = 30;

        if (ElTexto.text != " Ganaste! ")
        {
            ElTexto.text = "Game Over";
            Destroy(jugador);
        }

    }

}
